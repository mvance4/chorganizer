var path = require('path'),
    webpack = require('webpack');
module.exports = {
    entry: [
        'babel-polyfill',
        './app/main.js'
    ],
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'app.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                include: path.join(__dirname, 'app'),
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            }
        ]
    }
};
