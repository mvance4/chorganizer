import config from './main.config.js';
import routes from './main.routes.js';
import './controllers/module.js';
import './services/module.js';

angular.module('app', ['ngRoute', 'firebase', 'app.controllers', 'app.services'])
    .config(config)
    .config(routes);
