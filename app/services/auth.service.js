export default function auth($firebaseAuth) {
    this.baseRef = new Firebase('https://scorching-inferno-5281.firebaseio.com');
    return $firebaseAuth(this.baseRef);
}
