import auth from './auth.service.js';

export default angular.module('app.services', [])
    .factory('auth', auth);
