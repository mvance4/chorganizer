export default class organizerController {
    constructor(currentAuth, $firebaseArray, $location) {
        this.currentAuth = currentAuth;
        this.$location = $location;

        if (!this.currentAuth) {
            this.$location.path('/login');
        }
        
        this.baseRef = new Firebase('https://scorching-inferno-5281.firebaseio.com');
        this.usersRef = this.baseRef.child('users');
        this.choresRef = this.baseRef.child('chores');
        this.availableChoresQuery = this.choresRef.orderByChild('assigned').equalTo(false);
        this.userQuery = '';
        this.allUsers = $firebaseArray(this.usersRef);
        this.allChores = $firebaseArray(this.choresRef);
        this.users = [];

        this.allUsers.$loaded().then(() => {
            this.users = this.allUsers;

            this.allChores.$loaded().then(() => {
                this.availableChores = $firebaseArray(this.availableChoresQuery);

                for (var i = 0; i < this.users.length; i++) {
                    this.userQuery = this.choresRef.orderByChild('assignedTo').equalTo(this.users[i].$id);
                    this.users[i].chores = $firebaseArray(this.userQuery);
                }
            });
        });

    }

    assign(chore, assignee) {
        var record = this.allChores.$getRecord(chore.$id);
        if (assignee) {
            record.assigned = true;
            record.assignedTo = assignee;
        } else {
            record.assigned = false;
            record.assignedTo = '';
        }
        this.allChores.$save(record);
    }

    delete(chore) {
        var record = this.allChores.$getRecord(chore.$id);
        this.allChores.$remove(record);
    }
}
