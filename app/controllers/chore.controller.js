export default class choreController {
    constructor(currentAuth, $firebaseArray, $routeParams, $location) {
        this.currentAuth = currentAuth;
        this.$location = $location;

        if (!this.currentAuth) {
            this.$location.path('/login');
        }

        this.baseRef = new Firebase('https://scorching-inferno-5281.firebaseio.com');
        this.choresRef = this.baseRef.child('chores');
        this.usersRef = this.baseRef.child('users');
        this.choreList = $firebaseArray(this.choresRef);
        this.usersList = $firebaseArray(this.usersRef);

        this.choreList.$loaded().then(() => {
            this.chore = this.choreList.$getRecord($routeParams.id);
            this.usersList.$loaded().then(() => {
                this.chore.user = this.usersList.$getRecord(this.chore.assignedTo);
            });
        });
    }
}
