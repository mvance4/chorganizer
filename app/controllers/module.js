import choreController from './chore.controller.js';
import loginController from './login.controller.js';
import newChoreController from './newchore.controller.js';
import organizerController from './organizer.controller.js';

export default angular.module('app.controllers', [])
    .controller('choreController', choreController)
    .controller('loginController', loginController)
    .controller('newChoreController', newChoreController)
    .controller('organizerController', organizerController);
