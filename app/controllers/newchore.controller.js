export default class newChoreController {
    constructor(currentAuth, $firebaseArray, $location) {
        this.currentAuth = currentAuth;
        this.$location = $location;

        if (!this.currentAuth) {
            this.$location.path('/login');
        }

        this.baseRef = new Firebase('https://scorching-inferno-5281.firebaseio.com');
        this.choresRef = this.baseRef.child('chores');
        this.frequenciesRef = this.baseRef.child('frequencies');
        this.choresList = $firebaseArray(this.choresRef);

        this.name = '';
        this.description = '';
        this.frequencies = $firebaseArray(this.frequenciesRef);
        this.frequency = '';
    }


    addChore() {
        var chore = {
            name: this.name,
            description: this.description,
            frequency: this.frequency,
            assigned: false,
            assignedTo: ''
        };

        this.choresList.$add(chore);
        this.name = '';
        this.description = '';
        this.frequency = '';
        this.$location.path('/');
    }
}
