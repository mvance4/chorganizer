export default class loginController {
    constructor(auth, $firebaseArray, $location) {
        this.$location = $location;
        this.$firebaseArray = $firebaseArray;
        this.auth = auth;
        this.baseRef = new Firebase('https://scorching-inferno-5281.firebaseio.com');
        this.usersRef = this.baseRef.child('/users');
        this.allUsers = $firebaseArray(this.usersRef);

        this.getName = (authData) => {
            switch (authData.provider) {
                case 'google':
                    return authData.google.displayName;
                default:
                    console.error('provider not recognized');
            }
        };
    }


    login() {
        this.auth.$authWithOAuthPopup('google').then((authData) => {
            console.log('Logged in as: ', authData.uid);
            this.allUsers.$loaded().then(() => {
                this.userQuery = this.usersRef.orderByChild('oauthId').equalTo(authData.uid);
                this.user = this.$firebaseArray(this.userQuery);

                this.user.$loaded().then(() => {
                    if (!this.user.length > 0) {
                        this.allUsers.$add({
                            oauthId: authData.uid,
                            name: this.getName(authData)
                        });
                    }
                });
            });
            this.$location.path('/');
        });
    }
}
