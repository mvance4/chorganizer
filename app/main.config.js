export default function config($locationProvider) {
    $locationProvider.html5Mode(true);
}
