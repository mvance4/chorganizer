export default function routes($routeProvider) {
    $routeProvider.when('/login', {
        templateUrl: 'views/login.html',
        controller: 'loginController',
        controllerAs: 'login'
    }).when('/', {
        templateUrl: 'views/organizer.html',
        controller: 'organizerController',
        controllerAs: 'organizer',
        resolve: {
            'currentAuth': ['auth', function(Auth) {
                return Auth.$waitForAuth();
            }]
        }
    }).when('/newchore', {
        templateUrl: 'views/newchore.html',
        controller: 'newChoreController',
        controllerAs: 'newChore',
        resolve: {
            'currentAuth': ['auth', function(Auth) {
                return Auth.$waitForAuth();
            }]
        }
    }).when('/chore/:id', {
        templateUrl: 'views/chore.html',
        controller: 'choreController',
        controllerAs: 'chore',
        resolve: {
            'currentAuth': ['auth', function(Auth) {
                return Auth.$waitForAuth();
            }]
        }
    }).otherwise({
        redirectTo: '/'
    });
}
