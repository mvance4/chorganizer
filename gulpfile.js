var gulp = require('gulp'),
    concat = require('gulp-concat');

gulp.task('default', () => {

});

gulp.task('build', () => {
    return gulp.src(['./app/**/*.js'])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./public'));
});
