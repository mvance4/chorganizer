## setup

install nodejs(https://nodejs.org/)

run `npm install`

## Running Locally

run `npm start`

Your app should now be running on [localhost:5000](http://localhost:5000/).